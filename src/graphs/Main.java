package graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import java.lang.Math;

import view.View;

public class Main {

	public static void main(String[] args) {
		List<Double> scores = new ArrayList<>();
		
		String minDataPoints_str = JOptionPane.showInputDialog("Entre com o valor mínimo de x");
		int minValueX = Integer.parseInt(minDataPoints_str);
		
		String maxDataPoints_str = JOptionPane.showInputDialog("Entre com o valor máximo de x");
		int maxValueX = Integer.parseInt(maxDataPoints_str);
		
		String step_str = JOptionPane.showInputDialog("Entre com o passo");
		float step = Float.parseFloat(step_str);

        for (float i = minValueX; i < maxValueX; i = (float) (i + step)) {
            scores.add(Math.cos(i));
        }

		View view = new View(scores);
	}

}
